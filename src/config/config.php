<?php

return [

    /*
    |--------------------------------------------------------------------------
    | NullCountry Object
    |--------------------------------------------------------------------------
    |
    | Define if remove from collection NullCountry Objects.
    |
    */
    'nullCountry' => false,

];