<?php

namespace Rudashi\Countries;

use Illuminate\Support\Facades\Facade;

class CountriesFacade extends Facade
{

    protected static function getFacadeAccessor(): string
    {
        return Countries::class;
    }

}
