<?php

namespace Rudashi\Countries;

use Illuminate\Support\ServiceProvider;

class CountriesServiceProvider extends ServiceProvider
{

    public function boot(): void
    {
        $this->loadJsonTranslationsFrom(__DIR__ . '/lang/');
    }

    public function register(): void
    {
        $this->mergeConfigFrom(__DIR__ . '/config/config.php', 'countries');
    }

}
