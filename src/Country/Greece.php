<?php

namespace Rudashi\Countries\Country;

use Rudashi\Countries\Contracts\Country;

class Greece extends Country
{
    public string $code = 'el';

    public string $name = 'Greece';

    public string $currency = 'EUR';

}
