<?php

namespace Rudashi\Countries\Country;

use Rudashi\Countries\Contracts\Country;

class Denmark extends Country
{
    public string $code = 'dk';

    public string $name = 'Denmark';

    public string $currency = 'DKK';

}
