<?php

namespace Rudashi\Countries\Country;

use Rudashi\Countries\Contracts\Country;

class Estonia extends Country
{
    public string $code = 'ee';

    public string $name = 'Estonia';

    public string $currency = 'EUR';

}
