<?php

namespace Rudashi\Countries\Country;

use Rudashi\Countries\Contracts\Country;

class Hungary extends Country
{
    public string $code = 'hu';

    public string $name = 'Hungary';

    public string $currency = 'HUF';

}
