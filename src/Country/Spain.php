<?php

namespace Rudashi\Countries\Country;

use Rudashi\Countries\Contracts\Country;

class Spain extends Country
{
    public string $code = 'es';

    public string $name = 'Spain';

    public string $currency = 'EUR';

}
