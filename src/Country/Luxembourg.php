<?php

namespace Rudashi\Countries\Country;

use Rudashi\Countries\Contracts\Country;

class Luxembourg extends Country
{
    public string $code = 'lu';

    public string $name = 'Luxembourg';

    public string $currency = 'EUR';

}
