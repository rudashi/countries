<?php

namespace Rudashi\Countries\Country;

use Rudashi\Countries\Contracts\Country;

class Sweden extends Country
{
    public string $code = 'se';

    public string $name = 'Sweden';

    public string $currency = 'SEK';

}
