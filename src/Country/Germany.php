<?php

namespace Rudashi\Countries\Country;

use Rudashi\Countries\Contracts\Country;

class Germany extends Country
{
    public string $code = 'de';

    public string $name = 'Germany';

    public string $currency = 'EUR';

}
