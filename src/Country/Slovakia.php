<?php

namespace Rudashi\Countries\Country;

use Rudashi\Countries\Contracts\Country;

class Slovakia extends Country
{
    public string $code = 'sk';

    public string $name = 'Slovakia';

    public string $currency = 'EUR';

}
