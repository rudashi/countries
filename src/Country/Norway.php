<?php

namespace Rudashi\Countries\Country;

use Rudashi\Countries\Contracts\Country;

class Norway extends Country
{
    public string $code = 'no';

    public string $name = 'Norway';

    public string $currency = 'NOK';

}
