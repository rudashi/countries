<?php

namespace Rudashi\Countries\Country;

use Rudashi\Countries\Contracts\Country;

class Latvia extends Country
{
    public string $code = 'lv';

    public string $name = 'Latvia';

    public string $currency = 'EUR';

}
