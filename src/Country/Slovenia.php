<?php

namespace Rudashi\Countries\Country;

use Rudashi\Countries\Contracts\Country;

class Slovenia extends Country
{
    public string $code = 'si';

    public string $name = 'Slovenia';

    public string $currency = 'EUR';

}
