<?php

namespace Rudashi\Countries\Country;

use Rudashi\Countries\Contracts\Country;

class Lithuania extends Country
{
    public string $code = 'lt';

    public string $name = 'Lithuania';

    public string $currency = 'EUR';

}
