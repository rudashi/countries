<?php

namespace Rudashi\Countries\Country;

use Rudashi\Countries\Contracts\Country;

class Croatia extends Country
{
    public string $code = 'hr';

    public string $name = 'Croatia';

    public string $currency = 'HRK';

}
