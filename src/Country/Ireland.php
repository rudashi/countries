<?php

namespace Rudashi\Countries\Country;

use Rudashi\Countries\Contracts\Country;

class Ireland extends Country
{
    public string $code = 'ie';

    public string $name = 'Ireland';

    public string $currency = 'EUR';

}
