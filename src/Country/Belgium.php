<?php

namespace Rudashi\Countries\Country;

use Rudashi\Countries\Contracts\Country;

class Belgium extends Country
{
    public string $code = 'be';

    public string $name = 'Belgium';

    public string $currency = 'EUR';

}
