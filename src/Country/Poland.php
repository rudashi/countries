<?php

namespace Rudashi\Countries\Country;

use Rudashi\Countries\Contracts\Country;

class Poland extends Country
{
    public string $code = 'pl';

    public string $name = 'Poland';

    public string $currency = 'PLN';

}
