<?php

namespace Rudashi\Countries\Country;

use Rudashi\Countries\Contracts\Country;

class Portugal extends Country
{
    public string $code = 'pt';

    public string $name = 'Portugal';

    public string $currency = 'EUR';

}
