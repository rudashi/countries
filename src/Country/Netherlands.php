<?php

namespace Rudashi\Countries\Country;

use Rudashi\Countries\Contracts\Country;

class Netherlands extends Country
{
    public string $code = 'nl';

    public string $name = 'Netherlands';

    public string $currency = 'EUR';

}
