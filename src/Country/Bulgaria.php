<?php

namespace Rudashi\Countries\Country;

use Rudashi\Countries\Contracts\Country;

class Bulgaria extends Country
{
    public string $code = 'bg';

    public string $name = 'Bulgaria';

    public string $currency = 'BGN';

}
