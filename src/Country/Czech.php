<?php

namespace Rudashi\Countries\Country;

use Rudashi\Countries\Contracts\Country;

class Czech extends Country
{
    public string $code = 'cz';

    public string $name = 'Czech Republic';

    public string $currency = 'CZK';

}
