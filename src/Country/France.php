<?php

namespace Rudashi\Countries\Country;

use Rudashi\Countries\Contracts\Country;

class France extends Country
{
    public string $code = 'fr';

    public string $name = 'France';

    public string $currency = 'EUR';

}
