<?php

namespace Rudashi\Countries\Country;

use Rudashi\Countries\Contracts\Country;

class ElSalvador extends Country
{
    public string $code = 'sv';

    public string $name = 'El Salvador';

    public string $currency = 'USD';

}
