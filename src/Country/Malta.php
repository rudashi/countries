<?php

namespace Rudashi\Countries\Country;

use Rudashi\Countries\Contracts\Country;

class Malta extends Country
{
    public string $code = 'mt';

    public string $name = 'Malta';

    public string $currency = 'EUR';

}
