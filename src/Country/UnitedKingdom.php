<?php

namespace Rudashi\Countries\Country;

use Rudashi\Countries\Contracts\Country;

class UnitedKingdom extends Country
{
    public string $code = 'gb';

    public string $name = 'United Kingdom';

    public string $currency = 'GBP';

}
