<?php

namespace Rudashi\Countries\Country;

use Rudashi\Countries\Contracts\Country;

class Cyprus extends Country
{
    public string $code = 'cy';

    public string $name = 'Cyprus';

    public string $currency = 'EUR';

}
