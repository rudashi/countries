<?php

namespace Rudashi\Countries\Country;

use Rudashi\Countries\Contracts\Country;

class Finland extends Country
{
    public string $code = 'fi';

    public string $name = 'Finland';

    public string $currency = 'EUR';

}
