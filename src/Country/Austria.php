<?php

namespace Rudashi\Countries\Country;

use Rudashi\Countries\Contracts\Country;

class Austria extends Country
{
    public string $code = 'at';

    public string $name = 'Austria';

    public string $currency = 'EUR';

}
