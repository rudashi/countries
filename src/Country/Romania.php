<?php

namespace Rudashi\Countries\Country;

use Rudashi\Countries\Contracts\Country;

class Romania extends Country
{
    public string $code = 'ro';

    public string $name = 'Romania';

    public string $currency = 'RON';

}
