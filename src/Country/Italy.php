<?php

namespace Rudashi\Countries\Country;

use Rudashi\Countries\Contracts\Country;

class Italy extends Country
{
    public string $code = 'it';

    public string $name = 'Italy';

    public string $currency = 'EUR';

}
