<?php

namespace Rudashi\Countries\Enums;

use BenSampo\Enum\Enum;
use BenSampo\Enum\Contracts\LocalizedEnum;

class CurrencyType extends Enum implements LocalizedEnum
{

    public const PLN = 'zł';
    public const EUR = '€';
    public const GBP = '£';
    public const NOK = 'NOK';
    public const SEK = 'SEK';
    public const DKK = 'DKK';

}
