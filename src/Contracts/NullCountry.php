<?php

namespace Rudashi\Countries\Contracts;

class NullCountry extends Country
{

    public string $code = '';

    public string $name = '';

}
