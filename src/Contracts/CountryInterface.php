<?php

namespace Rudashi\Countries\Contracts;

interface CountryInterface
{

    public function setCode(string $code): Country;

    public function setName(string $name): Country;

    public function setCurrency(string $currency): Country;

    public function getCode(): string;

    public function getName(): string;

    public function getCurrency(): string;

    public function getCurrencySymbol(): ?string;

}
