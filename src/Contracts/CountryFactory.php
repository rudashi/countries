<?php

namespace Rudashi\Countries\Contracts;

use Rudashi\Countries\CountriesClassMap;
use Rudashi\Countries\Exceptions\InvalidCountryCodeException;

class CountryFactory
{
    protected string $code;

    protected CountryInterface $country;

    public function __construct(string $code, CountryInterface $country = null)
    {
        $this->setCode($code);
        $this->setCountry($country);
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function setCode(string $code): CountryFactory
    {
        $this->code = strtolower($code);

        return $this;
    }

    public function getCountry(): CountryInterface
    {
        return $this->country;
    }

    public function setCountry($country): CountryFactory
    {
        $this->country = $country;

        return $this;
    }

    private static function create(?string $code): CountryInterface
    {
        if (isset(CountriesClassMap::$classMap[strtolower($code)])) {
            return new CountriesClassMap::$classMap[strtolower($code)];
        }

        throw new InvalidCountryCodeException("Provided country code not exists. Provided: $code");
    }

    public static function set(string $code): CountryInterface
    {
        return (new CountryFactory($code, self::create($code)))->getCountry();
    }

}
