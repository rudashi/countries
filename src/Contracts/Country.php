<?php

namespace Rudashi\Countries\Contracts;

use Rudashi\Countries\Enums\CurrencyType;

abstract class Country implements CountryInterface
{
    public string $code;

    public string $name;

    public string $currency;

    public function getCode(): string
    {
        return $this->code;
    }

    public function setCode(string $code): Country
    {
        $this->code = $code;

        return $this;
    }

    public function getCurrency(): string
    {
        return $this->currency;
    }

    public function setCurrency(string $currency): Country
    {
        $this->currency = $currency;

        return $this;
    }

    public function getCurrencySymbol(): ?string
    {
        if (($this->currency !== null) && CurrencyType::hasKey($this->currency)) {
            return CurrencyType::getValue($this->currency);
        }
        return null;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): Country
    {
        $this->name = $name;

        return $this;
    }

    public function setCountry(array $country): Country
    {
        $this->setCode($country['code']);
        $this->setName($country['name']);

        return $this;
    }

}
