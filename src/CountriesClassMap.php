<?php

namespace Rudashi\Countries;

class CountriesClassMap
{
    public static array $classMap = [
        'at' => \Rudashi\Countries\Country\Austria::class,
        'be' => \Rudashi\Countries\Country\Belgium::class,
        'bg' => \Rudashi\Countries\Country\Bulgaria::class,
        'cy' => \Rudashi\Countries\Country\Cyprus::class,
        'cz' => \Rudashi\Countries\Country\Czech::class,
        'de' => \Rudashi\Countries\Country\Germany::class,
        'dk' => \Rudashi\Countries\Country\Denmark::class,
        'ee' => \Rudashi\Countries\Country\Estonia::class,
        'el' => \Rudashi\Countries\Country\Greece::class,
        'es' => \Rudashi\Countries\Country\Spain::class,
        'fi' => \Rudashi\Countries\Country\Finland::class,
        'fr' => \Rudashi\Countries\Country\France::class,
        'gb' => \Rudashi\Countries\Country\UnitedKingdom::class,
        'hr' => \Rudashi\Countries\Country\Croatia::class,
        'hu' => \Rudashi\Countries\Country\Hungary::class,
        'ie' => \Rudashi\Countries\Country\Ireland::class,
        'it' => \Rudashi\Countries\Country\Italy::class,
        'lt' => \Rudashi\Countries\Country\Lithuania::class,
        'lu' => \Rudashi\Countries\Country\Luxembourg::class,
        'lv' => \Rudashi\Countries\Country\Latvia::class,
        'mt' => \Rudashi\Countries\Country\Malta::class,
        'nl' => \Rudashi\Countries\Country\Netherlands::class,
        'pl' => \Rudashi\Countries\Country\Poland::class,
        'pt' => \Rudashi\Countries\Country\Portugal::class,
        'ro' => \Rudashi\Countries\Country\Romania::class,
        'se' => \Rudashi\Countries\Country\Sweden::class,
        'si' => \Rudashi\Countries\Country\Slovenia::class,
        'sk' => \Rudashi\Countries\Country\Slovakia::class,
        'sv' => \Rudashi\Countries\Country\ElSalvador::class,
    ];

}
