<?php

namespace Rudashi\Countries;

use Illuminate\Support\Collection;
use Rudashi\Countries\Contracts\Country;
use Rudashi\Countries\Contracts\CountryFactory;
use Rudashi\Countries\Contracts\CountryInterface;
use Rudashi\Countries\Contracts\NullCountry;
use Rudashi\Countries\Exceptions\InvalidCountryCodeException;
use Rudashi\Countries\Exceptions\InvalidCountryListException;

class Countries
{

    public Collection $countries;

    public function __construct(array $codes = [])
    {
        $this->countries = collect(CountriesList::$static_countries);
        $this->setCountries($this->countries);

        if ($codes) {
            $this->filter($codes);
        }
    }

    public function setCountries(Collection $countries): Countries
    {
        $countries->transform(static function ($country) {
            try {
                if (is_array($country) && array_key_exists('code', $country) && array_key_exists('name', $country)) {
                    return CountryFactory::set($country['code']);
                }
                throw new InvalidCountryListException('Provided country list is not correct. It should be: [ ["code" => "", "name" => ""] ]');
            } catch (InvalidCountryCodeException $e) {
                return (new NullCountry())->setCountry($country);
            }
        });

        $this->countries = config('countries.nullCountry') ? $countries : $countries->reject(static function ($value) {
            return $value instanceof NullCountry;
        });

        return $this;
    }

    public function filter(array $codes = []): Countries
    {
        $this->countries = $this->countries->filter(static function (Country $country) use ($codes) {
            return in_array($country->code, array_map('strtolower', $codes), true);
        });

        return $this;
    }

    public function find(string $code)
    {
        return $this->countries->firstWhere('code', $code);
    }

    public function all(): array
    {
        return $this->countries->all();
    }

    public static function instance(array $codes = []): Countries
    {
        return new self($codes);
    }

    public static function get(string $code): ?CountryInterface
    {
        return self::instance()->find(strtolower($code));
    }

    public static function set(array $codes = []): Countries
    {
        return self::instance($codes);
    }

}
