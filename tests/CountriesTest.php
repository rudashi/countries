<?php

namespace Rudashi\Countries;

use Illuminate\Support\Collection;
use Rudashi\Countries\Country\Poland;
use Rudashi\Countries\Country\UnitedKingdom;
use Rudashi\Countries\Contracts\NullCountry;
use Rudashi\Countries\Enums\CurrencyType;
use Rudashi\Countries\Exceptions\InvalidCountryListException;
use Tests\CreatesApplication;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

class CountriesTest extends BaseTestCase
{

    use CreatesApplication;

    private Countries $c;

    protected function setUp(): void
    {
        parent::setup();
        $this->c = new Countries();
    }

    public function test_set_countries_list_failed(): void
    {
        $this->expectException(InvalidCountryListException::class);

        $this->c->setCountries(
            collect(['at','pl','pt','se'])
        );
    }

    public function test_set_countries_list_missing_code_failed(): void
    {
        $this->expectException(InvalidCountryListException::class);

        $this->c->setCountries(
            collect([
                [ 'code' => 'de', 'name' => 'Germany'],
                [ 'code2' => 'se', 'name' => 'Sweden'],
                [ 'code' => 'pl', 'name' => 'Poland'],
            ])
        );
    }

    public function test_set_countries_list(): void
    {
        $this->c->setCountries(
            collect([
                [ 'code' => 'de', 'name' => 'Germany'],
                [ 'code' => 'pl', 'name' => 'Poland'],
                [ 'code' => 'se', 'name' => 'Sweden'],
            ])
        );
        $this->assertInstanceOf(Collection::class, $this->c->countries);
        $this->assertTrue($this->c->countries->isNotEmpty());
        $this->assertCount(3, $this->c->countries->toArray());
    }

    public function test_not_exists_set_countries_list(): void
    {
        $this->c->setCountries(
            collect([
                [ 'code' => 'gb', 'name' => 'Great Britain'],
                [ 'code' => 'en', 'name' => 'English'],
            ])
        );
        $this->assertInstanceOf(Collection::class, $this->c->countries);
        $this->assertTrue($this->c->countries->isNotEmpty());
        $this->assertCount(1, $this->c->countries->toArray());
    }

    public function test_set_countries_changed_config(): void
    {
        $this->app['config']->set('countries.nullCountry', true);

        $this->c->setCountries(
            collect([
                [ 'code' => 'gb', 'name' => 'Great Britain'],
                [ 'code' => 'en', 'name' => 'English'],
            ])
        );

        $this->assertInstanceOf(Collection::class, $this->c->countries);
        $this->assertTrue($this->c->countries->isNotEmpty());
        $this->assertCount(2, $this->c->countries->toArray());
        $this->assertInstanceOf(UnitedKingdom::class, $this->c->countries->offsetGet(0));
        $this->assertInstanceOf(NullCountry::class, $this->c->countries->offsetGet(1));
    }

    public function test_filter(): void
    {
        $this->c->filter(['pl', 'gb']);

        $this->assertInstanceOf(Collection::class, $this->c->countries);
        $this->assertTrue($this->c->countries->isNotEmpty());
        $this->assertCount(2, $this->c->countries->toArray());
    }

    public function test_get_country_failed(): void
    {
        $this->assertNull($this->c::get('pl2'));
    }

    public function test_get_country(): void
    {
        $poland = $this->c::get('pl');

        $this->assertNotNull($poland);
        $this->assertInstanceOf(Poland::class, $poland);
        $this->assertObjectHasAttribute('code', $poland);
        $this->assertObjectHasAttribute('name', $poland);
        $this->assertObjectHasAttribute('currency', $poland);
        $this->assertSame($poland->code, 'pl');
        $this->assertSame($poland->name, 'Poland');
        $this->assertSame($poland->currency, 'PLN');
    }

    public function test_get_country_currency_symbol(): void
    {
        $country = $this->c::get('pl');

        $this->assertNotNull($country->getCurrencySymbol());
        $this->assertSame($country->getCurrencySymbol(), CurrencyType::PLN);
    }

    public function test_get_country_currency_symbol_missing(): void
    {
        $country = $this->c::get('sv');

        $this->assertNull($country->getCurrencySymbol());
    }

    public function test_all(): void
    {
        $allCountries = $this->c->all();

        $this->assertIsArray($allCountries);
        $this->assertNotEmpty($allCountries);
    }

}
