Country Package
================

Package contain information about countries and their currency

## General System Requirements

- [PHP >7.4.0](http://php.net/)
- [Laravel ^5.* | ^6.*](https://github.com/laravel/framework)

## Quick Installation

If necessary, use the composer to download the library

```
$ composer require rudashi/countries
```

Remember to put repository in the composer.json

```
"repositories": [
    {
        "type": "vcs",
        "url":  "https://bitbucket.org/rudashi/countries.git"
    }
],
```

## Usage

### Dependency Injection

```php
public function __construct(\Rudashi\Countries\Countries $countries)
{
    $this->countries = $countries;
}
```

### Facade

```php
$countries = CountriesFacade::set([]);
```

### Examples

Filter Countries by codes.
```php
$this->countries->filter([]);
```

Get array of countries.
```php
$this->countries->all();
```

## Authors

* **Borys Żmuda** - Lead designer - [LinkedIn](https://www.linkedin.com/in/boryszmuda/), [Portfolio](https://rudashi.github.io/)
